<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EditCatController extends Controller
{
    /**
     * @Route("/edit/cat", name="edit_cat")
     */
    public function index()
    {
        return $this->render('edit_cat/index.html.twig', [
            'controller_name' => 'EditCatController',
        ]);
    }
}
