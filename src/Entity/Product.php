<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $category;

    /**
     * @ORM\Column(type="string", length=500)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\CartLine", mappedBy="relation")
     */
    private $cartLines_id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Category", inversedBy="products_id")
     * @ORM\JoinColumn(nullable=false)
     */
    private $relation;


    public function __construct()
    {
        $this->cartLines_id = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    public function getCategory(): ?string
    {
        return $this->category;
    }

    public function setCategory(string $category): self
    {
        $this->category = $category;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|CartLine[]
     */
    public function getCartLinesId(): Collection
    {
        return $this->cartLines_id;
    }

    public function addCartLinesId(CartLine $cartLinesId): self
    {
        if (!$this->cartLines_id->contains($cartLinesId)) {
            $this->cartLines_id[] = $cartLinesId;
            $cartLinesId->setRelation($this);
        }

        return $this;
    }

    public function removeCartLinesId(CartLine $cartLinesId): self
    {
        if ($this->cartLines_id->contains($cartLinesId)) {
            $this->cartLines_id->removeElement($cartLinesId);
            // set the owning side to null (unless already changed)
            if ($cartLinesId->getRelation() === $this) {
                $cartLinesId->setRelation(null);
            }
        }

        return $this;
    }

    public function getRelation(): ?Category
    {
        return $this->relation;
    }

    public function setRelation(?Category $relation): self
    {
        $this->relation = $relation;

        return $this;
    }

    

}
